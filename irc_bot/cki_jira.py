"""Bot Jira interface."""
import jira
from jira.client import GreenHopper


class Jira():
    """Helper class to interact with Jira."""

    def __init__(self, server, user, password, project='FASTMOVING'):
        """Create a new instance."""
        options = {
            'server': server,
            'mutual_authentication': 'DISABLED',
        }
        self.jira = jira.JIRA(options=options, basic_auth=(user, password))
        self.green_hopper = GreenHopper(
            options=options, basic_auth=(user, password))
        self.project = project
        self.board = None
        for board in self.green_hopper.boards():
            if board.name.startswith(self.project):
                self.board = board
                break

    def _get_issue(self, issue_id):
        if '-' not in issue_id:
            issue_id = f'{self.project}-{issue_id}'
        return self.jira.issue(issue_id)

    def get_current_sprint(self):
        """Return the current sprint."""
        for sprint in self.green_hopper.sprints(self.board.id):
            if sprint.state == 'ACTIVE':
                return sprint
        return None

    def assign_issue(self, issue_id, user):
        """Assign an issue to a user."""
        issue = self._get_issue(issue_id)
        if not self.jira.search_users(user):
            raise Exception(f'Invalid user {user}')
        self.jira.assign_issue(issue, user)

    def add_comment_to_issue(self, issue_id, comment):
        """Comment on an issue."""
        issue = self._get_issue(issue_id)
        self.jira.add_comment(issue, comment)

    def search_issues(self, pattern, project=None):
        """Search for issues."""
        project = project or self.project
        return self.jira.search_issues(f'project={project} AND {pattern}')

    def set_state(self, issue_id, new_state):
        """Transition an issue to a new state."""
        issue = self._get_issue(issue_id)
        if 'done' in new_state.lower():
            self.add_comment_to_issue(issue_id, 'Marking as done - Yayyy! 💘')
        elif 'in progress' in new_state.lower():
            sprint = self.get_current_sprint()
            if sprint:
                issue.update(fields={'customfield_10005': sprint.id})
        self.jira.transition_issue(issue, new_state)

    def set_estimate(self, issue_id, new_estimate):
        """Update the estimate of an issue."""
        issue = self._get_issue(issue_id)
        issue.update(fields={'customfield_10002': float(new_estimate)})

    def create_issue(self, summary, description, nickname, i_type='Task'):
        """Create a new issue."""
        description += f'\nCreated by {nickname}'
        issue = self.jira.create_issue(
            fields={
                'summary': summary,
                'description': description,
                'project': self.project,
                'issuetype': i_type,
            },
        )
        return issue
