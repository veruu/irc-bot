"""IRC message handlers."""
import os
import re
from collections import Counter
from datetime import timedelta
from distutils.util import strtobool

import sentry_sdk
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from dateutil.parser import parse as date_parse

from . import irc_message as ircm
from .queue import IRC_PUBLISH_QUEUE

LOGGER = get_logger('cki.irc_bot.amqp')


def colorize_status(status):
    """Return a colorized status."""
    if status == 'SUCCESS':
        color = 'lime'
    elif status == 'FAILED':
        color = 'red'
    else:
        color = 'silver'

    return ircm.style(status, fg=color, bold=True)


def last_stage_run(jobs):
    """Return the stage of the last run job."""
    return [x['stage'] for x in jobs[::-1] if x['status'] != 'skipped'][0]


def handle_sentry(hook_json):
    """Handle a Sentry alert."""
    if 'event' not in hook_json['data'] or \
            hook_json['action'] != 'triggered':
        return []
    event = hook_json['data']['event']

    project_name = re.sub('.*/([^/]+)/events/.*', r'\1', event['url'])
    message = event['title']
    url = misc.shorten_url(event['web_url'])

    message = f'😩 {project_name}: {message} - {url}'
    return [message]


def handle_pipeline(hook_json):
    # pylint: disable=too-many-statements,too-many-branches,too-many-locals
    """Handle a pipeline webhook."""
    pipeline_id = hook_json['object_attributes']['id']
    pipeline_status = hook_json['object_attributes']['status']

    # If the pipeline is in a pending state, don't say anything in IRC.
    if pipeline_status == 'pending':
        return []

    trigger_vars = {
        x['key']: x['value'] for x in
        hook_json['object_attributes']['variables']
    }

    # yaml error, pipeline could not be created and no variables are present
    # use a hack to try to detect retriggered pipelines in that case
    # this does not work for pipelines without a commit
    commit_message = misc.get_nested_key(hook_json, 'commit/message', '')
    if not trigger_vars and 'retrigger' in commit_message:
        return []

    cki_pipeline_branch = trigger_vars.get('cki_pipeline_branch')

    # Before doing anything, verify this is actually a repo and branch we want
    # to process.
    allowed_projects = {}
    allowed_projects_string = os.environ.get('PROJECTS')
    # An `if` is easier than trying to default to empty strings and then
    # working around those as split() on empty string returns ['']...
    if allowed_projects_string:
        for repo_branch in allowed_projects_string.split(','):
            repo, branch = repo_branch.split(':')
            allowed_projects.setdefault(repo, set())
            allowed_projects[repo].add(branch)

    # Are we filtering this repo?
    repo = trigger_vars.get('cki_project')
    if repo in allowed_projects:
        if cki_pipeline_branch not in allowed_projects[repo]:
            return []

    msg_prefix = ircm.style(f"#{pipeline_id}:", fg='blue')

    msg = f"{msg_prefix} {colorize_status(pipeline_status.upper())}"

    stages = hook_json['object_attributes']['stages']
    stage_jobs = {}
    stage_messages = []
    for stage in stages:
        if stage == 'review':
            continue

        jobs_in_stage = [
            x for x in hook_json['builds'] if x['stage'] == stage
            and x['status'] != 'skipped'
        ]
        sorted_jobs_in_stage = sorted(
            jobs_in_stage,
            reverse=True,
            key=lambda k: date_parse(k['created_at'])
        )
        stage_jobs[stage] = []
        for job in sorted_jobs_in_stage:
            if job['name'] not in [x['name'] for x in stage_jobs[stage]]:
                stage_jobs[stage].append(job)

        stage_count = Counter([x['status'] for x in stage_jobs[stage]])

        if stage_count['failed'] > 0:
            # At least one job failed in the stage.
            color = 'red'
        elif stage_count['running'] > 0:
            # At least one job in the stage is still running.
            color = 'silver'
        elif stage_count['skipped'] == len(stage_jobs[stage]):
            # All of the jobs in this stage were skipped!
            color = 'black'
        elif stage_count['success'] == len(stage_jobs[stage]):
            # All of the jobs passed.
            color = 'lime'
        else:
            color = None

        if color:
            stage_messages.append(ircm.style(stage[0].upper(), fg=color))
        else:
            stage_messages.append(stage[0].upper())

    msg += f" [{''.join(stage_messages)}]"

    # Skip retriggers
    if strtobool(trigger_vars.get('retrigger', 'false')):
        return []

    # If the pipeline duration is set, send that to IRC.
    if 'duration' in hook_json['object_attributes']:
        if hook_json['object_attributes']['duration']:
            duration = int(hook_json['object_attributes']['duration'])
            pretty_duration = str(timedelta(seconds=duration))
            msg += f" [{pretty_duration}]"

    # Add on extra trigger variables
    msg += f" {trigger_vars.get('title', 'Pipeline crash, no jobs created')} "

    # Add a shortened link to the pipeline page.
    project_baseurl = hook_json['project']['web_url']
    pipeline_url = f"{project_baseurl}/pipelines/{pipeline_id}"

    # Do not print internal URLs unless specified for internal bots. The URLs
    # themselves are fine to be public as they don't contain any confidential
    # info but they will confuse people because they can only be opened when
    # running in Red Hat network.
    print_urls = os.environ.get('PRINT_URLS', 'False')
    if print_urls == 'True':
        msg += f" {misc.shorten_url(pipeline_url)}"

        # Add a shortened link to the osci dashboard
        if cki_pipeline_branch == 'fedora':
            # Skip fedora, it isn't part of OSCI
            pass
        elif pipeline_status != 'running' and 'brew_task_id' in trigger_vars:
            host = 'https://dashboard.osci.redhat.com'
            task_id = trigger_vars.get('brew_task_id')
            osci_dash = f'{host}/#/artifact/brew-build/aid/{task_id}'
            msg += f" - osci dash {misc.shorten_url(osci_dash)}"

    return msg.split('\n')


def process_message(_, payload):
    """Process a webhook message."""
    if 'object_attributes' in payload:
        lines = handle_pipeline(payload)
    else:
        lines = handle_sentry(payload)
    for line in lines:
        IRC_PUBLISH_QUEUE.put(line)
    return lines


def main():
    """Run main loop."""
    rabbitmq_host = os.environ.get('RABBITMQ_HOST', 'localhost')
    rabbitmq_port = misc.get_env_int('RABBITMQ_PORT', 5672)
    rabbitmq_user = os.environ.get('RABBITMQ_USER', 'guest')
    rabbitmq_password = os.environ.get('RABBITMQ_PASSWORD', 'guest')

    webhooks_exchange = os.environ.get(
        'WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks')

    queue = MessageQueue(rabbitmq_host, rabbitmq_port,
                         rabbitmq_user, rabbitmq_password)

    if misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    queue.consume_messages(
        webhooks_exchange, os.environ['IRC_PIPELINE_ROUTING_KEYS'].split(),
        process_message, queue_name=os.environ.get('IRC_PIPELINE_QUEUE'))


if __name__ == "__main__":
    main()
