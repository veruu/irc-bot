# irc-bot

IRC bot hooked up to print pipeline status and deal with JIRA

## Deployment guide

The bot is meant to run as a service in a container (see the provided
Dockerfile). Please note that the URL shorteners are internal to Red Hat
infrastructure and will not work elsewhere -- feel free to fork the bot and use
your own ones.

## Environment variables

Add these to your deployment. `PROJECTS` and `PRINT_RULS` are optional, others
are mandatory for the bot to work. If you don't need parts of the functionality
(e.g. the JIRA integration), feel free to comment out the related code.

* `PROJECTS`: Filter which branches of a repository should be processed. Format:
`repo:branch,repo2:branch2` -- only `branch` of `repo` and `branch2` of `repo2`
will be processed. All branches of `repo3` will be processed. If the variable is
not present, all branches of all repositories the webhook is set up for will be
processed.

* `PRINT_URLS`: Can be `'True'` or `'False'`. Print or don't print the URLs of
the pipelines. Defaults to `'False'`; useful if running the bot in public IRC
channels where people can't access the internal URLs anyways.

* `JIRA_SERVER`: JIRA server URL.

* `JIRA_USERNAME`: Username of the bot.

* `JIRA_PASSWORD`: Paswsword of the bot.

* `GITLAB_PRIVATE_TOKEN`: Private token of the bot, used to edit list of
successes in the GitLab snippet.

* `GITLAB_SNIPPET`: URL of the GitLab snippet with a list of successes.

* `IRC_SERVER`: URL of the IRC server to connect to.

* `IRC_PORT`: Port of the IRC server to connect to.

* `IRC_CHANNEL`: Name of the channel to connect to.
